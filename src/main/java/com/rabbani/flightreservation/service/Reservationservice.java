package com.rabbani.flightreservation.service;

import com.rabbani.flightreservation.dto.ReservationRequest;
import com.rabbani.flightreservation.entities.Reservation;

public interface Reservationservice {
	
	public Reservation bookFlight(ReservationRequest request);

}
