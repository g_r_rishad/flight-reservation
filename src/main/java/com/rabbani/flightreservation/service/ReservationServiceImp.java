package com.rabbani.flightreservation.service;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rabbani.flightreservation.controllers.UserController;
import com.rabbani.flightreservation.dto.ReservationRequest;
import com.rabbani.flightreservation.entities.Flight;
import com.rabbani.flightreservation.entities.Passenger;

import com.rabbani.flightreservation.entities.Reservation;
import com.rabbani.flightreservation.repos.FlightRepository;
import com.rabbani.flightreservation.repos.PassengerRepository;

import com.rabbani.flightreservation.repos.ReservationRepository;
import com.rabbani.flightreservation.util.EmailUtil;
import com.rabbani.flightreservation.util.PDFGenerator;

@Service
public class ReservationServiceImp implements Reservationservice {

	@Autowired
	FlightRepository flightRepository;
	
	@Autowired
	PassengerRepository passengerRepository;
	
	@Autowired
	ReservationRepository reservationRepository;
	
	@Autowired
	PDFGenerator pdfGenerator;
	
	@Autowired
	EmailUtil emailUtil;
	
	private static final Logger LOGGER= org.slf4j.LoggerFactory.getLogger(ReservationServiceImp.class);
	
	
	@Override
	public Reservation bookFlight(ReservationRequest request) {
		
		LOGGER.info("Inside bookFlight()");
		
		Long flightId = request.getFlightId();
		LOGGER.info("Fetching Flight for flight Id:"+flightId);
		
		Flight flight = flightRepository.findById(flightId).get();
		
		Passenger passenger=new Passenger();
		passenger.setFirstName(request.getPassengerFirstName());
		passenger.setLastName(request.getPassengerLastName());
		passenger.setEmail(request.getPassengerEmail());
		passenger.setPhone(request.getPassengerPhone());
		LOGGER.info("Saving Passenger :"+passenger);
		Passenger savePassenger = passengerRepository.save(passenger);
		
		
		Reservation reservation =new Reservation();
		reservation.setFlight(flight);
		reservation.setPassenger(passenger);
		reservation.setCheckedIn(false);
		LOGGER.info("Saving The Reservation:"+reservation);
		
		Reservation savedReservation = reservationRepository.save(reservation);
		
		String filePath = "C:\\Users\\Nashim\\Desktop\\reservations\\reservation"+savedReservation.getId()+".pdf";
		
		LOGGER.info("Emailing The Itinary");
		pdfGenerator.generateItinerary(savedReservation, filePath);
		
		emailUtil.sendItinerary(passenger.getEmail(), filePath);
		
		
		return savedReservation;
	}

}
