package com.rabbani.flightreservation.controllers;


import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.rabbani.flightreservation.dto.ReservationRequest;
import com.rabbani.flightreservation.entities.Flight;
import com.rabbani.flightreservation.entities.Reservation;
import com.rabbani.flightreservation.repos.FlightRepository;
import com.rabbani.flightreservation.service.Reservationservice;

@Controller
public class ReservationController {
	
	@Autowired
	Reservationservice reservationService;
	
	@Autowired
	FlightRepository flightRepository;
	
	private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(ReservationController.class);
	
	@RequestMapping(value="/showCompleteReservation", method= {RequestMethod.POST,RequestMethod.GET})
	public String showCompleteReservation(@RequestParam("flightId") Long flightId, ModelMap modelMap) {
		
		LOGGER.info("showCompleteReservation()  invoked with the Flight Id:"+flightId );
		Flight flight = flightRepository.findById(flightId).get();
		modelMap.addAttribute("flight", flight);
		LOGGER.info("Flight iS:"+flight);
		return "completeReservation";
	}
	
	@RequestMapping(value="/completeReservation",method= {RequestMethod.POST,RequestMethod.GET})
	public String completeReservation(ReservationRequest request,ModelMap modelMap) {
		
		LOGGER.info("completeReservation() :"+request);
		
		Reservation reservation =reservationService.bookFlight(request);
		modelMap.addAttribute("msg", "Reservation Completed Successfully and Id is:"+reservation.getId());
		return "reservationConfirmation";
		
	}

}
