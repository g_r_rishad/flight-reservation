package com.rabbani.flightreservation.controllers;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.itextpdf.text.log.LoggerFactory;
import com.rabbani.flightreservation.entities.User;
import com.rabbani.flightreservation.repos.UserRepository;

@Controller
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
	private static final Logger LOGGER= org.slf4j.LoggerFactory.getLogger(UserController.class);
	
	
	@RequestMapping("/showReg")
	public String showRegistrationPage() {
		LOGGER.info("Inside showRegistrationPage()");
		return "login/registerUser";
	}
	
	@RequestMapping(value="/registerUser",method= {RequestMethod.POST,RequestMethod.GET} )
	public String Register(@ModelAttribute("user") User user) {
		LOGGER.info("Inside Registr()"+user);
		userRepository.save(user);
		return "login/login";
	}
	
	@RequestMapping("/showLogin")
	public String showLoginPage() {
		LOGGER.info("Inside showLoginPage()");
		return "login/login";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String login(@RequestParam("email") String email,@RequestParam("password") String password, ModelMap modelMap){
		
		LOGGER.info("Inside Login() and the emil is: "+email);
		
		User user = userRepository.findByEmail(email);
		if(user.getPassword().equals(password)){
			return "findFlights";
		}
		else{
			modelMap.addAttribute("msg", "Invalide User Name or Password. Please Try Again.");
		}
		return "login/login";
	}

}
