package com.rabbani.flightreservation.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rabbani.flightreservation.entities.Passenger;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {

}
