package com.rabbani.flightreservation.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rabbani.flightreservation.entities.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

}
